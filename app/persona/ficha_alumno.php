<html>
    <head>
        <title>ficha_alumno</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    </head>

    <body>
        <div class="p-2 bg-dark text-white">
            <h2>Upload Person</h2>
        </div>
        <br>
        <form method="post" action="ficha_alumno_view.php" class="container border p-3 my-2 m-2 float-left" enctype="multipart/form-data">
            Name:
            <br><input type="text" name="personName"/>
            <br>Surname: 
            <br><input type="text" name="personSurname"/>
            <br>Adress: 
            <br><input type="text" name="personAddress"/>
            <br>Comments: 
            <br><input type="text" name="personComments"/>
            <br>Picture: 
            <br><input type="file" name="personPicture"/><br>
            <br><input type="submit" value="Send"/>
        </form>
    </body>
</html> 