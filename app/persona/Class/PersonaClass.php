<?php
    class Persona {
        private $name;
        private $surname;
        private $address;
        private $picture; //route of the file
        private $comment;

    /*
    * Setters. Para añadir y modificar valores
    */
        public function setName($paramName){
            $this->name = $paramName;
        }
        public function setSurname($paramSurname){
            $this->surname = $paramSurname;

        }
        public function setAddress($paramAdress){
            $this->address = $paramAdress;

        }
        public function setPicture($paramPicturePath){
            $this->picture = $paramPicturePath;
        }
        public function setComment($paramComment){
            $this->comment = $paramComment;
        }

    /*
    * Getters. Lo que quiere decir que los atributos de la clase son private
    */
        public function getName(){
            return $this->name;
        }
        public function getSurname(){
            return $this->surname;
        }
        public function getAddress(){
            return $this->address;
        }
        public function getPicture(){
            return $this->picture;
        }
        public function getComment(){
            return $this->comment;
        }

    }
?>