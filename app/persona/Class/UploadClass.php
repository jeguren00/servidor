<?php
  /*
  * OPCIONAL:
  * Clase personalizada extendida de Exception que utilizaremos para lanzar errores
  * en la subida de archivos. Por ejemplo:
  * throw new UploadError("Error: Please select a valid file format.");
  class UploadError extends Exception{}
  */
  define("ROUTE", "./fotos");
  class Upload {
    private $fileName;
    private $tmpFileName;

    /*
    * Constructor: Inicia la subida del archivo
    * Entrada:
    *   $name: Nombre del elemento del formulario del que gestionamos el $_FILE
    */
    function __construct($fileName,$tmpFileName){
      $this->fileName = $fileName;
      $this->tmpFileName = $tmpFileName;
      //echo $fileName."2";
      //echo $this->fileName."3";
      if (isset($this->fileName)) {
        $this->upload();
      }
    }

    /*
    * upload: Función que hace las operaciones necesarias para subir el archivo
    * al servidor
    */
    public function upload(){
      //$tempfilename = $_FILES[$this->fileName]["tmp_name"];
      //echo ROUTE.'/'.$this->fileName;
      //echo $_SERVER['DOCUMENT_ROOT'].$this->tmpFileName;
      //echo $this->fileName;
      move_uploaded_file($this->tmpFileName, ROUTE.'/'.$this->fileName);
      //move_uploaded_file($tempfilename, ROUTE."/".$this->fileName);
      /*
      try {move_uploaded_file($_FILES[$this->name]["tmp_name"], ROUTE . $filename);
      move_uploaded_file($_FILES[$this->name]["tmp_name"], ROUTE . $filename);
        if (file_exists(RUTA . $filename)) {
          throw new UploadError("Este archivo ya existe");
        }
      }catch (ExcepUploadErrortion $e) {
        echo "Error de subida: " . $e->getMessage();
      } catch (Exception $e) {
        echo $e->getMessage();
      }*/
    }
    
    /*
    * Getters. Lo que quiere decir que los atributos de la clase son private
    */
    //returns the new path of the image
    public function getPath(){
      $filename = $this->fileName;
      return ROUTE."/".$filename;
    }
  }
?>