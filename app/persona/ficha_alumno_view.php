<html>
    <head>
        <title>ficha_alumno</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    </head>

    <body>
        <div class="p-2 bg-dark text-white">
            <h2>Upload Person</h2>
        </div>
        <br>
        <?php
            //classes declared to construct the final pages with them
            include('Class/UploadClass.php');
            include('Class/PersonaClass.php');

            //attributes recived form the form, saved into vars to create the object user
            $name = $_POST["personName"];
            $surname = $_POST["personSurname"];
            $address = $_POST["personAddress"];
            $comment = $_POST["personComments"];
            $photoName = $_FILES["personPicture"]["name"];
            $photoTempName = $_FILES["personPicture"]["tmp_name"];

            //declaration of new objects, and adding of his attributes
            //new oject user, with the personal information
            $user = new Persona();
            //object upload to proces the sended image, and return the new path inside the folder fotos
            $upload = new Upload($photoName,$photoTempName);
            //set attributes of the object user
            $user->setName($name);
            $user->setSurname($surname);
            $user->setAddress($address);
            $user->setComment($comment);
            $user->setPicture($upload->getPath());

        ?>
        <div class="container p-3 my-3 border ">
            <img class="container p-3 my-3 border " src=<?php echo $user->getPicture()?>>
            <p class="container p-3 my-3 border "><?php echo $user->getName()?></p>
            <p class="container p-3 my-3 border "><?php echo $user->getSurname()?></p>
            <p class="container p-3 my-3 border "><?php echo $user->getAddress()?></p>
            <p class="container p-3 my-3 border "><?php echo $user->getComment()?></p>
        </div>
        
    </body>
</html> 

