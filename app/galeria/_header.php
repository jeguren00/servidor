<html>
    <head>
        <title>Image's Gallery</title>
        <meta charset="utf-8">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    </head>

    <div class="p-2 bg-dark text-white" onclick="location.href='index.php'">
        <h2>Image's Gallery</h2>
    </div>
    <br>
    <?php
        $state= $_GET["state"];
        $msg = $_GET["msg"];
        if ($state == "succes") {?>
            <div class="alert alert-success" role="alert">
            Imagen añadida correctamente
            </div>
        <?}
        else if($state == "error"){?>
            <div class="alert alert-danger" role="alert">
                <?php echo $msg;?>
            </div>
        <?}