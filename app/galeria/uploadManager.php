<?php
    include('Class/uploadClass.php');
    /*
    * Clase personalizada extendida de Exception que utilizaremos para lanzar errores
    * en la subida de archivos. Por ejemplo:
    * throw new UploadError("Error: Please select a valid file format.");
    */
    class UploadError extends Exception{
    }
    
    //main code block for direct operations
    try {
        if(isset($_FILES["uploadedImage"]) && $_FILES["uploadedImage"]["error"] == 0){
            //data to we need to upload the photo
            $photoName = $_FILES["uploadedImage"]["name"];
            $photoTempName = $_FILES["uploadedImage"]["tmp_name"];
            //data we need to test if the file is correct, and make the exceptions
            $filetype = $_FILES["uploadedImage"]["type"];
            $ext = pathinfo($photoName, PATHINFO_EXTENSION);        
            $allowed = array("jpg", "jpeg", "gif", "png");

            //if the file has and unwanted extension
            if(!in_array($ext, $allowed)){
                throw new UploadError("Error:Archivo de tipo no deseado");
            }            
            //to test if is inside photos and assure
            if(file_exists("./fotos/" . $photoName)){
                throw new UploadError("ERROR: ".$photoName." ya esta subido en la galeria");
            }
            //normal flow of the page if is all ok
            //title added to the picture by the user
            $imageTitle = $_POST["pictureName"];
            //if the image has no title throw exception
            if (empty($imageTitle)) {
                throw new UploadError("ERROR: Imagen sin titulo");
            }
            if (!is_writable("./fotos")) {
                throw new UploadError("ERROR: Carpeta fotos sin permisos");
            }

            //the image path
            //$imagePath = uploadPicture($photoName, $photoTempName);
            //add image to the database
            //addPictureToFile($imagePath,$imageTitle);
            
            $upload = new Upload($photoName,$imageTitle,$photoTempName);

            header('Location:index.php?state=succes');

        } else {
            throw new UploadError("Error: Problema en el archivo");
        }
    } catch (UploadError $e) {
        header('Location:index.php?state=error&msg='.$e->getMessage()); 
    }
?>