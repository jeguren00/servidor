<?php include_once('_header.php') ?>
<html>
    <head>
        <title>Image's Gallery</title>
        <meta charset="utf-8">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    </head>

    <body>
        <div class="container border p-3 my-2 m-2 float-left">
            <h2> Upload Picture</h2>
            <form method="post" action="uploadManager.php" enctype="multipart/form-data">
                Name:
                <br><input type="text" name="pictureName"/>
                <br>Picture: 
                <br><input type="file" name="uploadedImage" class="form-control"/><br>
                <br><input type="submit" value="Upload" class="btn btn-primary"/>
            </form>
        </div>
    </body>
</html>