<?php

define("ROUTE", "./fotos");
define("FILEDB", "fotos.txt");
class Upload {
    private $fileName;
    private $photoTitle;
    private $fileTempRoute;
    private $fileRoute;

    function __construct($fileName,$photoTitle,$fileTempRoute){
        $this->$fileName = $fileName;
        $this->$photoTitle = $photoTitle;
        $this->$fileTempRoute = $fileTempRoute;
        $this->fileRoute = $this->uploadPicture($this->$fileTempRoute,$this->$fileName);
        $this->addPictureToFile($this->fileRoute,$this->$photoTitle);
    }
    
    /*
    * Función que se encarga de subir un archivo y moverlo a la carpeta /pictures
    * que almacena todas las fotos.
    * Return: Devuelve la ruta final del archivo.
    */
    function uploadPicture($fileTempRoute,$fileName){
        move_uploaded_file($fileTempRoute, ROUTE."/".$fileName);
        //returns the final route of the images
        return ROUTE."/".$fileName;
    }

    /*
    * Función que se encarga de añadir al archivo fotos.txt el titulo y la ruta de la
    * fotografía recien subida
    * Entradas:
    *       $file_uploaded: La ruta del archivo
    *       $title_uploaded: El titulo del archivo
    * Return: null
    */
    function addPictureToFile($file_uploaded,$title_uploaded){
        $file = fopen(FILEDB, "a");
        fputs($file, "\n".$title_uploaded.'###'.$file_uploaded."\n");
        fclose($file);
        return true;
    }


}
?>