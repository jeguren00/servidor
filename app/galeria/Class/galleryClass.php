<?php
    include('Class/pictureClass.php');
    class Gallery {
        private $_gallery = [];
        private $filePath;

        /*Constructor: Recibe la ruta del archivo fotos.txt*/
        function __construct($fileName){
            if (isset($fileName) ) {
                $this->filePath = $fileName;
                $this->loadGallery();
            }
        }

        /*
        *Recorre el archivo fotos.txt y para cada linea, crea un
        *elemento Picture que lo añade al atributo $_gallery[]
        */
        function loadGallery(){
            //opens the file to read its content
            $file = fopen($this->filePath, "r");
            //the line of the file now iterated
            $stringFromFile = "";

            while (!feof($file)) {
                //grabs one line
                $stringFromFile = fgets($file);
                //extracts the position of the delimiter inside the fotos.txt
                $positionOfDelimiter = strpos($stringFromFile,"###");
                //with the delimiter and the line substracts, separatly the title and the pathe of the image
                $titleImage = substr($stringFromFile,0, $positionOfDelimiter);
                $routeImage = substr($stringFromFile, ($positionOfDelimiter + 3));
                
                //creates a new object picture with the information writed above
                $picture = new Picture($titleImage,$routeImage);
                //puts the picture inside the array
                array_push($this->_gallery,$picture);
            }
            //closes the file
            fclose($file);
        }

        /*
        *Getters.
        */
        public function getGallery(){
            return $this->_gallery;
        }
}
?>