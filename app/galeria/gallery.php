<?php include_once('_header.php') ?>

</style>
<body>
    
        <?php
            //includes the classes needed to run the program
            include('Class/galleryClass.php');
            //creates a new gallery with the photos saved inside the "database" fotos.txt
            $galleryObject = new Gallery ("./fotos.txt");
            //grabs the array with the pictures from the gallery
            $galleryPictures = $galleryObject->getGallery();
            $count = 0;
            
            //for to iterate all the pictures inside the array
            foreach ($galleryPictures as $valor) {
                if ($count==0) {
                    echo "<div class=\"row p-5\">";
                }
                //to not create an empty div if there is a blank line in fotos.txt
                if (!empty($valor->getTitle())) {
                    ?>
                    <!--
                    div container to save 
                    generates and img with the image form the database
                    generates a subtitle form the subtitle atribut of picture
                    -->
                    <div class = "col-3 card p-3 m-3">
                        <img class = "img-fluid d-flex" src =<?=$valor->getFileName()?>>
                        <div class="card-body">
                            <h3 class="card-body" ><?=$valor->getTitle()?></h3>
                        </div>
                        <br>
                    </div>
                    <?
                }
                if ($count==2) {
                    echo "</div>";
                    $count = 0;
                } else {
                    $count++;
                }
            }?>
</body>