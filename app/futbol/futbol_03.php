<!DOCTYPE html>
<html lang="en">
  <head>
    <title>jugadors i partits</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  </head>

  <body>
    <table class="table">
      <?php
        //array with the scores of each player
        $partitArray = $_POST['scores'];
        //array with the names of the players
        $nomJugadorArray = $_POST["nomJugadorores"];
        //var with the number of matches, with the player in it.
        $numPartits = $_POST["partits"];
        //names of the players
        $numJugadors = $_POST["jugadors"];

        //vars to save the lengths of the arrays, to use by for's limit
        $lengthPartitArrayJugador =  count($nomJugadorArray);
        $lengthPartitArrayResultat = count($partitArray[0]);

        //for to create the header of the table
        echo "<tr>";
        echo "<th>Jugador \t</th>";
        for ($i=1; $i <= $numPartits; $i++) {
          echo "<th>Partit $i \t</th>";
        }
        echo "</tr>";
        //for to create the interior of the table and fill it
        for ($i=0; $i < $lengthPartitArrayJugador; $i++) {
          echo "<tr>";
          echo "<td> ".$nomJugadorArray[$i]." \t</td>";
          for ($j=0; $j < $lengthPartitArrayResultat ; $j++) {
            echo "<td> ".$partitArray[$i][$j]." \t</td>";
          }
          echo "</tr>";
        }
      ?>  
    </table>
  <body>
</html>