<html>
    <head>
        <title>prod_list_02</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    </head>

    <body>
        <div class="p-2 bg-dark text-white">
            <h2>Product's List</h2>
        </div>
        <br>
        <form method="post" action="prod_list_03.php" class="container border p-3 my-2 m-2 float-left">
            <?php
                //import the number of products, that you want to enter
                $numProductos = $_POST["numProductos"];
                //for to generate the inputs
                for ($i=0; $i < $numProductos; $i++) {
            ?>
            Product Name: <input type="text" name="pName[]"/>
            Price: <input type="text" name="pPrice[]"/> <br> <br>
            <?php }?>

            <input type="hidden" name="tableSize" value="<?php echo $numProductos ?>">
            <input type="submit" value="Send"/>
        </form>
    </body>
</html>