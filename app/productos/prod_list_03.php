<html>
    <head>
        <title>prod_list_03</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    </head>

    <body>
        <div class="p-2 bg-dark text-white">
            <h2>Product's List</h2>
        </div>
        <br>
        <div class="container">
            <table class="table table-hover">
                <?php
                    //import the name of the products, and his prices
                    $pName = $_POST["pName"];
                    $pPrice = $_POST["pPrice"];
                    //the size of the table
                    $tableSize = $_POST["tableSize"];
                    //counter for the list index
                    $count = 1;
                ?>  
                <tr>
                    <th>#</th>
                    <th>Product Name</th>
                    <th>Price</th>
                </tr>

                <?php
                    //for to create the rows for the products inputed
                        for ($i=0; $i < $tableSize; $i++) { 
                            echo "<tr>";
                            //if there is no product in the array or an incomplete one
                            if ($pName[$i] == '' || $pPrice[$i] == '') {
                                echo "<td>Product not inserted</td>";
                                echo "<td></td>";
                                echo "<td></td>";
                            } else {
                                //in case all is ok
                                echo "<td>$count</td>";
                                echo "<td>$pName[$i]</td>";
                                echo "<td>$pPrice[$i]</td>";
                                $count++;
                            }
                            echo "</tr>";
                        }
                    ?>
            </table>
        <div>
    </body>
</html>